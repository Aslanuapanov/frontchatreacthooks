import React, { useEffect } from 'react'
import { Switch, Route, useHistory } from 'react-router-dom'
import './App.scss'
import Auth from './auth/Auth'
import Chat from './chat/Chat'
function App() {
  const history = useHistory();
  const token = localStorage.getItem('token');
  useEffect(() => {
    token == null ? history.push('/Auth') : history.push('/Chat')
  })
  return (
      <div className="App">
        <div className="App_body">
          <Switch>
            <Route path="/Auth" component={Auth} />
            <Route path="/Chat" component={Chat} />
          </Switch>
        </div>
      </div>
  );
}

export default App;
