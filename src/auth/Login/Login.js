import React, { useState } from 'react'
import { Link, useHistory, withRouter } from 'react-router-dom'
import './Login.scss'
import axios from 'axios'

function Login() {
  const history = useHistory()
  const [username, setUsername] = useState('')
  const [userpassword, setPassword] = useState('')

  const onChangeLogin = e => {
    setUsername(e.target.value);
  }

  const onChangePassword = e => {
    setPassword(e.target.value);
  }

  const login = () => {
    axios.post('http://localhost:3001/auth/login', {
      username,
      password: userpassword,
    })
    .then(res => {
      localStorage.setItem('token', res.data.token)
      localStorage.setItem('username', res.data.user.username)
      if (localStorage.getItem('token') !== null) {
        history.push('/Chat')
      } else {
        history.push('/')
      }
    })
    
  }

  return (
    <div className="Login">
      <div className="Login__title">
        <h2>MyChat</h2>
      </div>
      <div className="Login__block">
        <div className="Login__block__title">
          <h3>SignIn for MyChat</h3>
        </div>
        <div className="Login__block__form">
          <div className="Login__block__form__inputs">
            <span>Email</span>
            <input onChange={onChangeLogin} type="text" placeholder="Your Email" />
          </div>
          <div className="Login__block__form__inputs">
            <span>Password</span>
            <input onChange={onChangePassword} type="password" placeholder="Your Password" />
          </div>
        </div>
      </div>
      <button onClick={login} className="Login__btn">Login</button>
      <Link className="Registration__btn" to="/Auth/Registration">Registration</Link>
    </div>
  )
}

export default withRouter(Login)
