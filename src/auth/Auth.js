import React from 'react'
import {Switch, Route } from 'react-router-dom'
import Login from './Login/Login'
import Registration from './Registration/Registration'

function Auth() {
  return (
    <div>
      <Switch>
        <Route exact path="/Auth" component={Login} />
        <Route exact path="/Auth/Registration" component={Registration} />
      </Switch>
    </div>
  )
}

export default Auth
