import React, { useState } from 'react'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'

function Registration() {
  const [username, setUsername] = useState('')
  const [userpassword, setPassword] = useState('')

  const onChangeLogin = e => {
    setUsername(e.target.value);
  }

  const onChangePassword = e => {
    setPassword(e.target.value);
  }

  const register = () => {
    axios.post('http://localhost:3001/auth/register', {
      username,
      password: userpassword,
    })
  }
  
  return (
    <div className="Registration">
      <div className="Registration__block">
        <div className="Registration__block__title">
          <h3>Registration for MyChat</h3>
        </div>
        <div className="Registration__block__form">
          <div className="Registration__block__form__inputs">
            <span>Email</span>
            <input onChange={onChangeLogin} type="text" placeholder="Your Email" />
          </div>
          <div className="Registration__block__form__inputs">
            <span>Password</span>
            <input onChange={onChangePassword} type="password" placeholder="Your Password" />
          </div>
        </div>
      </div>
      <button onClick={register} className="Registration__btn">Registration</button>
      <Link to="/Auth" className="Login__btn">Login</Link>
    </div>
  )
}

export default withRouter(Registration)
