import React from 'react'
import avatar from './../../assets/img/avatar.jpg'
import ChatIcon from './../../assets/img/ChatIcon.svg'
import BellIcon from './../../assets/img/bell.svg'
import GridIcon from './../../assets/img/grid.svg'
import PersonIcon from './../../assets/img/person.svg'
import SettingsIcon from './../../assets/img/Settings.svg'
import LogoutIcon from './../../assets/img/logout.svg'
import { Link } from 'react-router-dom'

function ChatNavbar() {
  const logout = () => {
    localStorage.removeItem('token')
  }

  return (
    <div className="ChatNavbar">
      <div className="ChatNavbar__avatar">
        <img src={avatar} alt="avatar" />
      </div>
      <div className="ChatNavbar__username">
        <h2>User Name</h2>
      </div>
      <div className="ChatNavbar__navigate">
        <ul>
          <li>
            <Link to="/Chat/home">
              <img src={GridIcon} alt="icon" />
              <span>Home</span>
            </Link>
          </li>
          <li>
            <Link to="/Chat/main">
              <img src={ChatIcon} alt="icon" />
              <span>Chat</span>
            </Link>
          </li>
          <li>
            <Link to="/Chat/contact">
              <img src={PersonIcon} alt="icon" />
              <span>Contact</span>
            </Link>
          </li>
          <li>
            <Link to="/Chat/notifications">
              <img src={BellIcon} alt="icon" />
              <span>Notifications</span>
            </Link>
          </li>
          <li>
            <Link to="/Chat/settings">
              <img src={SettingsIcon} alt="icon" />
              <span>Settings</span>
            </Link>
          </li>
          <li className="ChatNavbar__navigate__logout" onClick={logout}>
            <Link to="/">
              <img src={LogoutIcon} alt="icon" />
              <span>Log out</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default ChatNavbar
