import React from 'react'
import Dots from '../../../assets/img/horizontaldots.svg'

function anotherUser(props) {
  return (
    <div className="ChatMain__body__wrap__anotheruser">
      <div className="ChatMain__body__wrap__anotheruser__img">
        {/* <img /> */}
      </div>
      <div className="ChatMain__body__wrap__anotheruser__descr">
        <p>{props.message}</p>
      </div>
      <button className="ChatMain__body__wrap__anotheruser__dots">
        <img src={Dots} />  
      </button>
    </div>
  )
}

export default anotherUser
