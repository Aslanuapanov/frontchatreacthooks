import React from 'react'
import Dots from '../../../assets/img/horizontaldots.svg'
import ViewMessage from '../../../assets/img/view.svg'
import ViewedMessage from '../../../assets/img/viewed.svg'

function defaultUser(props) {
  return (
    <div className="ChatMain__body__wrap__defaultuser">
      <div className="ChatMain__body__wrap__defaultuser__dots">
        <img src={Dots} alt="dots" />
      </div>
      <div className="ChatMain__body__wrap__defaultuser__descr">
        <p>{props.message}</p>
      </div>
      <div className="ChatMain__body__wrap__defaultuser__status">
        <img src={ViewMessage} alt="ViewMessage" />
      </div>
    </div>
  )
}

export default defaultUser
