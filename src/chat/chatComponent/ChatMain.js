import React, {useState, useEffect, useRef} from 'react'
import AnotherUser from './chatMainComponent/anotherUser'
import DefaultUser from './chatMainComponent/defaultUser'
import shareIcon from '../../assets/img/share.svg'
import dotsIcon from '../../assets/img/dots.svg'
import AddIcon from '../../assets/img/plus.svg'
// import FilmIcon from '../../assets/img/film.svg'
// import FileIcon from '../../assets/img/file.svg'
// import ImageIcon from '../../assets/img/image.svg'
import SmileIcon from '../../assets/img/SmileIcon.svg'
import SubmitIcon from '../../assets/img/submit.svg'
import io from 'socket.io-client';
import axios from 'axios';

function ChatMain(props) {
  const socket = io('http://localhost:3002');
  const [message, setMessage] = useState('');
  const username = localStorage.getItem('username');
  const [messageData, setMessagedata] = useState([]);
  const messagesEndRef = useRef(null)
  const _isMounted = useRef(true);
  const {match} = props;
  const contactData = props.location.state;

  const scrollToBottom = () => {
    return messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }

  const createMessage =  () => {
    socket.on('newMessage', (data) => {
      setMessagedata([...messageData, data]);
    })
    if (message.length > 0) {
      axios({
        method: 'post',
        url: 'http://localhost:3001/message/createmessage',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {
          username,
          message,
          chatId: match.params.id
        }
      })
      setMessage('')
    }
    scrollToBottom()
  }

  useEffect(() => {
    socket.on('connection', (data) => {
      console.log(data);
    });
  }, []);

  useEffect(() => {
    return () => {
      _isMounted.current = false;
    }
  }, [])

  useEffect(() => {
    axios({
      method: 'post',
      url: 'http://localhost:3001/message/usersMessage',
      data: {
        chatId: match.params.id
      }
    }).then(res => {
        if (_isMounted.current) {
          setMessagedata([...res.data])
        }
      })
  }, [])

  useEffect(scrollToBottom, [messageData])

  return (
    <div className="ChatMain">
      <div className="ChatMain__head">
        <div className="ChatMain__head__user">
          <div className="ChatMain__head__user__img">
            {/* <img src="" /> */}
          </div>
          <div className="ChatMain__head__user__descr">
            <h2>{contactData.contact.username}</h2>
            <h3>last online 5 hours ago</h3>
          </div>
        </div>
        <div className="ChatMain__head__btn">
          <button>
            <img src={shareIcon} alt="sharebtns" />
          </button>
          <button>
            <img src={dotsIcon} alt="dotsbtns" />
          </button>
        </div>
      </div>
      <div className="ChatMain__body">
        <div className="ChatMain__body__wrap">
          {
            messageData.map(element => {
              return element.username === username ? <DefaultUser key={element._id} message={element.message}  /> : <AnotherUser key={element._id} message={element.message}  />  
            })
          }
          <div ref={messagesEndRef} />
        </div>
      </div>
      <div className="ChatMain__foot">
        <div className="ChatMain__foot__files">
          <button className="ChatMain__foot__files__add">
            <img src={AddIcon} alt="add" />
          </button>
        </div>
        <div className="ChatMain__foot__input">
          <input onChange={e => {setMessage(e.target.value)}} value={message} placeholder="Type a message here" />
        </div>
        <div className="ChatMain__foot__btns">
          <button className="ChatMain__foot__btns__smile">
            <img src={SmileIcon} alt="Smile" />
          </button>
          <button onClick={createMessage} className="ChatMain__foot__btns__submit">
            <img src={SubmitIcon} alt="Submit" />
          </button>
        </div>
      </div>
    </div>
  )
}

export default ChatMain
