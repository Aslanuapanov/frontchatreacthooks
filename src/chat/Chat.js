import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ChatNavbar from './chatComponent/ChatNavbar';
import ChatsNew from './notificationComponent/ChatsNew';
import ChatHome from './homeComponent/ChatHome';
import ChatContact from './contactComponent/chatContact';
import ChatSetting from './settingComponent/chatSetting';
import ChatMain from './chatComponent/ChatMain';
function Chat() {
  return (
    <div style={styles.ChatWrapper}>
      <ChatNavbar />
      <Switch>
        <Route exact path="/Chat" component={ChatHome} />
        <Route path="/Chat/Notification" component={ChatsNew} />
        <Route path="/Chat/Contact" component={ChatContact} />
        <Route path="/Chat/Setting" component={ChatSetting} />
        <Route path="/Chat/Main/:id" render = {(props) => { 
                    return (<ChatMain {...props} />)
        }} />
      </Switch>
    </div>
  )
}

const styles = {
  ChatWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    flexDirection: 'row',
    height: '100vh'
  }
}

export default Chat
