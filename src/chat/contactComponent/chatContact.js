import React, { useEffect, useState, useRef } from 'react'
import ChatContactItems from './chatContactItems'
import axios from 'axios';

function ChatContact() {
  const [contacts, setContacts] = useState([]);
  const _isMounted = useRef(true);

  useEffect(() => {
    axios.get('http://localhost:3001/auth')
      .then(res => {
        if (_isMounted.current) {
          setContacts([...res.data])
        }
      })
  }, [])

  return (
    <div className='ChatContact'>
      <div className='ChatContact__title'>
        <h3>My contacts</h3>
      </div>
      <div className='ChatContact__row'>
        {
          contacts.map(item => {
            return <ChatContactItems key={item._id} contact={item} />
          })
        }
      </div>
    </div>
  )
}

export default ChatContact
