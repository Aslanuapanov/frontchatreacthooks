import axios from 'axios';
import React from 'react';
import {useHistory} from 'react-router-dom'

function ChatContactItems(contact) {
  const history = useHistory()

  const createSendMessage = () => {
    axios({
      url: 'http://localhost:3001/message/usersMembers',
      method: 'post',
      data: {
        users: [localStorage.getItem('username'), contact.contact.username]
      }
    }).then(res => history.push(`/Chat/Main/${contact.contact._id}`, { contact: contact.contact }))
  }

  return (
    <div className="ChatContact__row_items">
      <div className="ChatMain__head__user" style={Styles.chatContact}>
        <div className="ChatMain__head__user__img"></div>
        <div className="ChatMain__head__user__descr">
          <h2>{contact.contact.username}</h2>
          <h3>last online 5 hours ago</h3>
        </div>
      </div>
      <button onClick={createSendMessage} className="ChatContact__row_items__btn">
        <span>Send message</span>
      </button>
    </div>
  )
}

export default ChatContactItems

const Styles = {
  chatContact: {
    'paddingLeft': '20px',
    'height': '100%'
  }
}